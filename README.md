# Rotary Broach Tool

This is a alternative to the [Hemingway Compact Rotary
Broach](https://www.hemingwaykits.com/HK2570) design which can
be made with the stock provided in the Hemingway kit, plus one
additional 6800 bearing (beyond the one provided in the kit), one
5F-12M thrust bearing, and one shaft of your choice (e.g. cut from
the end of a damaged end mill or drill, or merely making the shaft
shorter than the Hemingway plans call for).

The purpose of this alternative design is to avoid axial loading
through a radial bearing, as used in the Hemingway design. Not using
the provided material for a shaft, or even merely making the shaft
smaller, provides enough stock for the sleeve that carries load
between the two radial bearings.

The detailed machining instructions for the kit will, with
modifications that should be generally obvious, apply to these
drawings.

Americans with easier access to 5/16 drill rod can bore and
ream the hole to 5/16 rather than 8mm and use self-supplied
5/16 stock instead of the small amount of 8mm stock supplied,
that is less readily available in the US. This is true both
for the original kit and this alternative design.

An alternative configuration for longer 1.25" broaches is shown in
the model as the "8mmx1.25" configuration of the Broach object in
the FreeCAD file and the associated drawings. This is essentially
identical except for the screw hole offsets on the back of the main
body. Instead of a 0.70mm offset 8.30 mm and 9.70 mm, it uses a
0.77mm offset 8.23 mm and 9.77 mm respectively. That is, it is biased
toward one side by 0.07mm more to account for the longer broaches.

It also shows an alternative ".5x1.75" configuration for 1/2"
broaches that will require larger stock, two MR6702 radial bearings,
and one F5-12M thrust bearing. It uses an offset of 0.99m due to the
longer stock length. As of the time of writing, this configuration
has not been tested, and the drawings have not been confirmed to
produce a working assembly.

## Warning

These parts were modeled in a develpment version of FreeCAD. They
may fail in arbitrary ways if you try to use them.
